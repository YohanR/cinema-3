<?php

/* 
vous ajouterez ici les fonctions qui vous sont utiles dans le site,
je vous ai créé la première qui est pour le moment incomplète et qui devra contenir
la logique pour choisir la page à charger
*/

function getContent() {
	if(isset($_GET['exercice1'])) {
		include __DIR__.'/../pages/exercice1.php';
	} if(isset($_GET['exercice2'])) {
		include __DIR__.'/../pages/exercice2.php';
	} if(isset($_GET['exercice3'])) {
		include __DIR__.'/../pages/exercice3.php';
	} if(isset($_GET['exercice4'])) {
		include __DIR__.'/../pages/exercice4.php';
	} if(isset($_GET['exercice5'])) {
		include __DIR__.'/../pages/exercice5.php';
	} if(isset($_GET['exercice6'])) {
		include __DIR__.'/../pages/exercice6.php';
	} if(isset($_GET['exercice7'])) {
		include __DIR__.'/../pages/exercice7.php';
	} 
     if(isset($_GET['bonus'])) {
		include __DIR__.'/../pages/bonus.php';
	} 
     if(isset($_GET['modif'])) {
		include __DIR__.'/../pages/modif.php';
	} 
     if(isset($_GET['home'])) {
		include __DIR__.'/../pages/home.php';
	} 
}

function getPart($name){
	include __DIR__ . '/../parts/'. $name . '.php';
}

function getUserData() {
	$file = file_get_contents("../data/user.json");
	$user =	json_decode($file);
	echo $user->name . " " . $user->first_name . " " . $user->occupation . "</br>";
	foreach($user->experiences as $value){
	  echo $value->company . " : " . $value->year . "</br>" ;
	
	
	}
	
	
}
function lastMessage() {
	$data = file_get_contents('../data/last_message.json');
	$dataJson = json_decode($data, true);
	$name = $dataJson['user_name'];
	$mail = $dataJson['user_mail'];
	$message = $dataJson['user_message'];
	echo "<div class='lastMessage'> De la part de  $name  <br>  $mail  <br>  Message : $message <br> <a href='/?home'>Cliquer ici pour retourner a l'acceuil !</a> </div>";
	
}

